#ifndef CPPUNIT_UI_TEXT_TESTRUNNER_H
#define CPPUNIT_UI_TEXT_TESTRUNNER_H

#include <comb_tmp_cppunit/ui/text/TextTestRunner.h>


#if defined(CPPUNIT_HAVE_NAMESPACES)

CPPUNIT_NS_BEGIN
namespace TextUi
{

  /*! Text TestRunner (DEPRECATED).
   * \deprecated Use TextTestRunner instead.
   */
  typedef TextTestRunner TestRunner;

}
CPPUNIT_NS_END

#endif // defined(CPPUNIT_HAVE_NAMESPACES)


#endif  // CPPUNIT_UI_TEXT_TESTRUNNER_H
