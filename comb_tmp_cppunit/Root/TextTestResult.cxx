#include <comb_tmp_cppunit/Exception.h>
#include <comb_tmp_cppunit/Test.h>
#include <comb_tmp_cppunit/TestFailure.h>
#include <comb_tmp_cppunit/TextTestResult.h>
#include <comb_tmp_cppunit/TextOutputter.h>
#include <comb_tmp_cppunit/portability/Stream.h>


CPPUNIT_NS_BEGIN


TextTestResult::TextTestResult()
{
  addListener( this );
}


void 
TextTestResult::addFailure( const TestFailure &failure )
{
  TestResultCollector::addFailure( failure );
  stdCOut() << ( failure.isError() ? "E" : "F" );
}


void 
TextTestResult::startTest( Test *test )
{
  TestResultCollector::startTest (test);
  stdCOut() << ".";
}


void 
TextTestResult::print( OStream &stream ) 
{
  TextOutputter outputter( this, stream );
  outputter.write();
}


OStream &
operator <<( OStream &stream, 
             TextTestResult &result )
{ 
  result.print (stream); return stream; 
}


CPPUNIT_NS_END
