# Combination builder for CDI files

Combination routines for CDI files, waiting to be migrated to [atlas-ftag-calibration](https://gitlab.cern.ch/atlas-ftag-calibration).
# Installation

First download the repository:
```shell
git clone https://gitlab.cern.ch/mstamenk/combination-temporary.git
```

Then, in order to set the environment up and execute the tests:
```shell
source build.sh
```
Finally, run the combination and CDI files production via:
```shell
cd comb_CalibrationResults
rm -rf *.html
rm -rf *.tar.gz
source scripts/runall.sh
```
# Architecture

```shell
comb_CalibrationResults         # contains Calibration steering files + results of combination
comb_Combination                # c++ scripts to run the combination, plot, extrapolation ...
comb_PostProcessingTools        # formerly NPandSmoothingTools
comb_tmp_cppunit                # temporarily here until homogeneization with Atlas cppunit test
comb_tmp_TestPolicy             # temporarily here until homogeneization with Atlas TestPolicy
```

# Running the combination

Setting up atlas

```shell
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
```
or 
```shell
setupATLAS                      # on lxplus
```

Setup rootcore release
```shell
rcSetup Base,2.3.35             # or newer
```
Do setup and build
```shell
rc find_packages
rc compile
```
Perform routine tests 
```shell
rc test_ut --package Combination
```
Perform the combination.

```shell
cd CalibrationResults
rm -rf *.html
rm -rf *.tar.gz
source scripts/runall.sh
```
# Continuous integration

Continous integration via Gitlab is not supported yet for Atlas. For this reason, the current project doesn't implement continuous integration. 

Jenkins is favoured for such purpose, please refer to [ATLAS Software Documentation : CI Services](https://atlassoftwaredocs.web.cern.ch/MRtutorial/ci/). Such a feature will be implemented.

# Example of routines to build the CDI files

This example is performed on the bjets ttbar calibrations analyses. Please refer to JIRA ticket [AFT-295](https://its.cern.ch/jira/browse/AFT-295) for an update on the task.

Fetch the the calibration analyses (sent per mail to [atlas-cp-flavtag-CDI-updates](mailto:atlas-cp-flavtag-CDI-updates@cern.ch). In the case of the btag-ttbar (16 files):

```shell
btag_ttbarPDF_v1.0_21-2-10_DL1_FixedCutBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_DL1_FixedCutBEff_70.txt
btag_ttbarPDF_v1.0_21-2-10_DL1_FixedCutBEff_77.txt
btag_ttbarPDF_v1.0_21-2-10_DL1_FixedCutBEff_85.txt

btag_ttbarPDF_v1.0_21-2-10_DL1_HybBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_DL1_HybBEff_70.txt
btag_ttbarPDF_v1.0_21-2-10_DL1_HybBEff_77.txt
btag_ttbarPDF_v1.0_21-2-10_DL1_HybBEff_85.txt

btag_ttbarPDF_v1.0_21-2-10_MV2c10_FixedCutBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_MV2c10_FixedCutBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_MV2c10_FixedCutBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_MV2c10_FixedCutBEff_60.txt

btag_ttbarPDF_v1.0_21-2-10_MV2c10_HybBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_MV2c10_HybBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_MV2c10_HybBEff_60.txt
btag_ttbarPDF_v1.0_21-2-10_MV2c10_HybBEff_60.txt
```

Then, place them in the appropriate repertory. For bjets-ttbar:
```shell
comb_CalibrationResults/analyses/2017-21-13TeV/bjets/ttbar_pdf/
```




Add the recommendations as done in the [commit](https://github.com/AtlasBID/CalibrationResults/commit/3c0b1b6f285a058ba755578d1a3ab1e136ad18b4) and remove the pre-recommendations from steering file. Please refer to this [commit](https://github.com/AtlasBID/CalibrationResults/commit/e13668b58d251d67cbec08092328e46967ed52d8) on previous [AtlasBID](https://github.com/AtlasBID) repository. The location of the steering file can be found here:

```shell
comb_CalibrationResults/analyses/2017-21-13TeV/2017-21-13TeV.py
```

Modify the binning of the extrapolation files. Please refer to the [commit](https://github.com/AtlasBID/CalibrationResults/commit/bb2af8dda5bc14603a9dc24b9c592b2326e4bcd6) for more details. The binning of the extrapolations should match the one provided by the calibrations analyses. The extrapolation files can be found at:

```shell
comb_CalibrationResults/analyses/2017-21-13TeV/extrap/
``` 

Finally, modify the defaults files in order to take into account the new recommendation and write it in the final root file. Please refer to this [commit](https://github.com/AtlasBID/CalibrationResults/commit/170ec9692ed5f1916e1e1f224fa764c97bb657c2)

```shell
comb_CalibrationResults/analyses/2017-21-13TeV/defaults.txt
``` 

Create the efficiency maps and working points for all jet collections [based on this](https://github.com/AtlasBID/CalibrationResults/commit/4f5aa8bb30f5df4c763bbd16ea019da376511155).

Artificially create the AntiKt2PV0TrackJets, AntiKtVR30Rmax4Rmin02TrackJets , AntiKt4EMPFlowJets by copying AntiKt4EMTopoJets and customly modifying the input files. This will be done by a python script following this [commit](https://github.com/AtlasBID/CalibrationResults/commit/9055dfee37fe7f97da339dbf17e19e2e1d77b38a).



