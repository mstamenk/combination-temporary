#include <comb_tmp_cppunit/BriefTestProgressListener.h>
#include <comb_tmp_cppunit/Test.h>
#include <comb_tmp_cppunit/TestFailure.h>
#include <comb_tmp_cppunit/portability/Stream.h>


CPPUNIT_NS_BEGIN


BriefTestProgressListener::BriefTestProgressListener()
    : m_lastTestFailed( false )
{
}


BriefTestProgressListener::~BriefTestProgressListener()
{
}


void 
BriefTestProgressListener::startTest( Test *test )
{
  stdCOut() << test->getName();
  stdCOut().flush();

  m_lastTestFailed = false;
}


void 
BriefTestProgressListener::addFailure( const TestFailure &failure )
{
  stdCOut() << " : " << (failure.isError() ? "error" : "assertion");
  m_lastTestFailed  = true;
}


void 
BriefTestProgressListener::endTest( Test * )
{
  if ( !m_lastTestFailed )
    stdCOut()  <<  " : OK";
  stdCOut() << "\n";
}


CPPUNIT_NS_END

