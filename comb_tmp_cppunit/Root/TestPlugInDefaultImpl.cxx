#include <comb_tmp_cppunit/config/SourcePrefix.h>

#if !defined(CPPUNIT_NO_TESTPLUGIN)

#include <comb_tmp_cppunit/TestSuite.h>
#include <comb_tmp_cppunit/extensions/TestFactoryRegistry.h>
#include <comb_tmp_cppunit/plugin/TestPlugInDefaultImpl.h>


CPPUNIT_NS_BEGIN


TestPlugInDefaultImpl::TestPlugInDefaultImpl() 
{
}


TestPlugInDefaultImpl::~TestPlugInDefaultImpl()
{
}


void 
TestPlugInDefaultImpl::initialize( TestFactoryRegistry *,
                                   const PlugInParameters & )
{
}


void 
TestPlugInDefaultImpl::addListener( TestResult * )
{
}


void 
TestPlugInDefaultImpl::removeListener( TestResult * )
{
}


void 
TestPlugInDefaultImpl::addXmlOutputterHooks( XmlOutputter * )
{
}


void 
TestPlugInDefaultImpl::removeXmlOutputterHooks()
{
}


void 
TestPlugInDefaultImpl::uninitialize( TestFactoryRegistry * )
{
}


CPPUNIT_NS_END


#endif // !defined(CPPUNIT_NO_TESTPLUGIN)
